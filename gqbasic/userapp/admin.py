from django.contrib import admin
from .models import user

# Register your models here.


@admin.register(user)
class PageAdmin(admin.ModelAdmin):
    list_display = ["name", "gender"]
