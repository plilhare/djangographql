import graphene
from graphene_django import DjangoObjectType
from .models import user


class UserType(DjangoObjectType):
    class Meta:
        model = user


class Query(graphene.ObjectType):
    users = graphene.List(UserType)

    def resolve_users(self, info, **kwargs):
        return user.objects.all()
