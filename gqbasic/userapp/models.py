from django.db import models


class user(models.Model):
    name = models.CharField(max_length=100)
    gender = models.CharField(
        max_length=100, choices=(("M", "Male"), ("F", "Female")), default="M"
    )
