import graphene
import userapp.schema


class Query(userapp.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
