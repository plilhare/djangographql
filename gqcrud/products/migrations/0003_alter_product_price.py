# Generated by Django 3.2 on 2021-04-09 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0002_alter_product_price"),
    ]

    operations = [
        migrations.AlterField(
            model_name="product",
            name="price",
            field=models.FloatField(),
        ),
    ]
